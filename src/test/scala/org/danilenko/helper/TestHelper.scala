package org.danilenko.helper

import org.danilenko.task.{Empty, Leaf, Node}

object TestHelper {
  /**
    * Create node for test's
    * @param nodesCount - amount of created nodes
    * @return - created Node
    */
  def createFakeNode(nodesCount: Int): Node = {
    require(10 <= nodesCount && nodesCount <= 1000)
    def helper(node: Node, i: Int): Node = {
      if (i == 10)
        node
      else
        Leaf(nodesCount - 1, nodesCount - 1, List(node))
    }
    helper(Empty, nodesCount)
  }
}
